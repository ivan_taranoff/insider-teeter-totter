import { IPoint } from "@/types/IPoint";
import { Origin } from "@/types/Origin";
import { WeightType } from "@/types/WeightType";

export interface IWeight {
  origin: Origin;
  type: WeightType;
  location: IPoint;
  weight: number;
  width: number;
  height: number;
  falling: boolean;
  id: number | null;
  rotation: number;
}

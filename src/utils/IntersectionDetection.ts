import { IPoint } from "@/types/IPoint";
import { IWeight } from "@/types/IWeight";

export class IntersectionDetection {
  public static lineRect(
    // polygon in fact
    lineStart: IPoint,
    lineEnd: IPoint,
    rectanglePoints: IPoint[]
  ): IPoint[] | null {
    // check if the line has hit any of the rectangle's sides
    // uses the Line to Line intersection function below
    const intersections = rectanglePoints.reduce(
      (acc: IPoint[], e: IPoint, i: number): IPoint[] => {
        const result = IntersectionDetection.lineLine(
          lineStart,
          lineEnd,
          rectanglePoints[i],
          rectanglePoints[i < rectanglePoints.length - 1 ? i + 1 : 0]
        );
        if (result) acc.push(result);
        return acc;
      },
      []
    );

    // if ANY of the above are true, the line
    // has hit the rectangle
    if (intersections.length) {
      return intersections;
    }
    return null;
  }

  public static rectRect(firstRect: IPoint[], secondRect: IPoint[]) {
    const intersections = firstRect.reduce(
      (acc: IPoint[], e: IPoint, i: number): IPoint[] => {
        const result = IntersectionDetection.lineRect(
          firstRect[i],
          firstRect[i < firstRect.length - 1 ? i + 1 : 0],
          secondRect
        );
        if (result) acc.push(...result);
        return acc;
      },
      []
    );
    if (intersections.length) {
      return intersections;
    }
    return null;
  }

  public static lineLine(
    firstStart: IPoint,
    firstEnd: IPoint,
    secondStart: IPoint,
    secondEnd: IPoint
  ): IPoint | null {
    firstEnd.left; // x
    secondEnd.top; // y
    // calculate the direction of the lines
    const uA =
      ((secondEnd.left - secondStart.left) *
        (firstStart.top - secondStart.top) -
        (secondEnd.top - secondStart.top) *
          (firstStart.left - secondStart.left)) /
      ((secondEnd.top - secondStart.top) * (firstEnd.left - firstStart.left) -
        (secondEnd.left - secondStart.left) * (firstEnd.top - firstStart.top));
    const uB =
      ((firstEnd.left - firstStart.left) * (firstStart.top - secondStart.top) -
        (firstEnd.top - firstStart.top) *
          (firstStart.left - secondStart.left)) /
      ((secondEnd.top - secondStart.top) * (firstEnd.left - firstStart.left) -
        (secondEnd.left - secondStart.left) * (firstEnd.top - firstStart.top));

    // if uA and uB are between 0-1, lines are colliding
    if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
      let intersectionX =
        firstStart.left + uA * (firstEnd.left - firstStart.left);
      let intersectionY = firstStart.top + uA * (firstEnd.top - firstStart.top);
      return {
        left: intersectionX,
        top: intersectionY
      };
      // return true;
    }
    return null;
  }

  public static getWeightPoints(weight: IWeight) {
    return [
      { left: weight.location.left, top: weight.location.top },
      { left: weight.location.left, top: weight.location.top + weight.height },
      {
        left: weight.location.left + weight.width,
        top: weight.location.top + weight.height
      },
      { left: weight.location.left + weight.width, top: weight.location.top }
    ];
  }
}

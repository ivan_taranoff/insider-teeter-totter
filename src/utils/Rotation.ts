import { IPoint } from "@/types/IPoint";

export class Rotation {
  public static rotate(
    pivot: IPoint,
    point: IPoint,
    angle: number // clockwise
  ): IPoint {
    const radians = (Math.PI / 180) * angle;
    const cos = Math.cos(radians);
    const sin = Math.sin(radians);
    const nx =
      cos * (point.left - pivot.left) +
      sin * (point.top - pivot.top) +
      pivot.left;
    const ny =
      cos * (point.top - pivot.top) -
      sin * (point.left - pivot.left) +
      pivot.top;
    return { left: nx, top: ny };
  }
}

import { IWeight } from "@/types/IWeight";
import { IPoint } from "@/types/IPoint";
import { GameOverReason } from "@/types/GameOverReason";

export interface RootState {
  weights: IWeight[];
  plankAngle: number;
  plankWidth: number;
  plankHeight: number;
  pivotPoint: IPoint;
  momentumLeft: number;
  momentumRight: number;
  rotationRate: number;
  fallingRate: number;
  shiftValue: number;
  isPaused: boolean;
  worldTickRate: number; // milliseconds
  blockTimer: number;
  maxPlankAngle: number;
  maxMomentumDifference: number;
  gameOver: boolean;
  gameOverReason: GameOverReason;
  moveLeft: boolean;
  moveRight: boolean;
}

import { MutationTree } from "vuex";
import { RootState } from "@/store/RootState";
import { IWeight } from "@/types/IWeight";
import { Rotation } from "@/utils/Rotation";
import { initialStoreState } from "@/store/store";

export const rootMutations: MutationTree<RootState> = {
  reset(state) {
    Object.assign(state, initialStoreState);
    state.weights = [];
  },
  addWeight(state, payload: IWeight) {
    if (payload.id === null) {
      payload.id = state.weights.length;
    }
    state.weights.push(payload);
  },
  updateMomentums(state) {
    state.momentumLeft = state.weights
      .filter(e => !e.falling && e.origin === "player")
      .reduce((acc: number, w: IWeight) => {
        return acc + (state.pivotPoint.left - w.location.left) * w.weight;
      }, 0);
    state.momentumRight = state.weights
      .filter(e => !e.falling && e.origin === "npc")
      .reduce((acc: number, w: IWeight) => {
        return acc + (w.location.left - state.pivotPoint.left) * w.weight;
      }, 0);
  },

  updateRotation(state) {
    const momentumDifference = state.momentumLeft - state.momentumRight;
    if (momentumDifference > state.maxMomentumDifference) {
      state.gameOver = true;
      state.gameOverReason = "overweight";
    }
    const modifier =
      (momentumDifference / state.maxMomentumDifference) * state.rotationRate;
    state.plankAngle -= modifier;
    if (
      state.plankAngle > state.maxPlankAngle ||
      state.plankAngle < state.maxPlankAngle * -1
    ) {
      state.gameOver = true;
      state.gameOverReason = "angle";
    }

    // update all non-falling weights rotation
    state.weights
      .filter(w => !w.falling)
      .forEach((w: IWeight) => {
        w.location = Rotation.rotate(state.pivotPoint, w.location, modifier);
        w.rotation -= modifier;
      });
  },
  setMomentumLeft(state, payload: number) {
    state.momentumLeft = payload;
  },
  setMomentumRight(state, payload: number) {
    state.momentumRight = payload;
  },
  pause(state) {
    state.isPaused = true;
  },
  unpause(state) {
    state.isPaused = false;
  },
  moveLeft(state, payload) {
    state.moveLeft = payload;
  },
  moveRight(state, payload) {
    state.moveRight = payload;
  },
  move(state) {
    state.weights
      .filter(w => w.falling && w.origin === "player")
      .forEach(w => {
        if (state.moveRight) w.location.left += state.shiftValue;
        if (state.moveLeft) w.location.left -= state.shiftValue;
        if (w.location.left + w.width > 50) {
          w.location.left = 50 - w.width;
        }
        if (w.location.left < 0) w.location.left = 0;
      });
  }
};

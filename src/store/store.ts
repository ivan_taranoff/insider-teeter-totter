import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import { RootState } from "@/store/RootState";
import { rootActions } from "@/store/rootActions";
import { rootMutations } from "@/store/rootMutations";
import { rootGetters } from "@/store/rootGetters";

Vue.use(Vuex);

export const initialStoreState = {
  weights: [],
  plankAngle: 0,
  momentumLeft: 0,
  momentumRight: 0,
  fallingRate: 1, // world coordinates - decimeters
  rotationRate: 0.5, // max degrees per tick
  maxMomentumDifference: 200, // kg*decimeter
  isPaused: false,
  worldTickRate: 20, // milliseconds
  maxPlankAngle: 16.704,
  plankWidth: 100, // world coordinates - decimeters
  plankHeight: 5,
  shiftValue: 3,
  pivotPoint: {
    left: 50,
    top: 175
  },
  gameOver: false,
  gameOverReason: null,
  moveLeft: false,
  moveRight: false,
  blockTimer: 3000
};

const storeDefinition: StoreOptions<RootState> = {
  state: {
    ...initialStoreState
  },
  actions: rootActions,
  mutations: rootMutations,
  getters: rootGetters
};

export const store = new Vuex.Store<RootState>(storeDefinition);

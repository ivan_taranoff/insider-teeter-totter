import { GetterTree } from "vuex";
import { RootState } from "@/store/RootState";
import { IPoint } from "@/types/IPoint";
import { Rotation } from "@/utils/Rotation";

export const rootGetters: GetterTree<RootState, any> = {
  plankDegree(state): string {
    return state.plankAngle + "deg";
  },
  plankTopLine(state): IPoint[] {
    const points: IPoint[] = [
      {
        left: state.pivotPoint.left - state.plankWidth / 2,
        top: state.pivotPoint.top
      },
      {
        left: state.pivotPoint.left + state.plankWidth / 2,
        top: state.pivotPoint.top
      }
    ];

    return points.map(v =>
      Rotation.rotate(state.pivotPoint, v, state.plankAngle)
    );
  },
  plankPoints(state, getters): IPoint[] {
    const points: IPoint[] = [
      {
        left: state.pivotPoint.left + state.plankWidth / 2,
        top: state.pivotPoint.top + state.plankHeight
      },
      {
        left: state.pivotPoint.left - state.plankWidth / 2,
        top: state.pivotPoint.top + state.plankHeight
      }
    ];

    const rotatedPoints = points.map(v =>
      Rotation.rotate(state.pivotPoint, v, state.plankAngle)
    );
    return [...getters.plankTopLine, ...rotatedPoints];
  }
};

import { ActionTree } from "vuex";
import { RootState } from "@/store/RootState";
import { Origin } from "@/types/Origin";
import { IWeight } from "@/types/IWeight";
import { WeightType } from "@/types/WeightType";
import { IntersectionDetection } from "@/utils/IntersectionDetection";

export const rootActions: ActionTree<RootState, any> = {
  createWeight({ commit, state }, payload: Origin): void {
    if (state.gameOver) return;
    const weight = Math.ceil(Math.random() * 9);
    const width = weight * 3;
    let left = Math.random() * (50 - width);
    if (payload === "npc") {
      left += 50;
    }
    const type: WeightType = <WeightType>(
      ["triangle", "rectangle", "circle"][Math.floor(Math.random() * 3)]
    );
    // noinspection JSSuspiciousNameCombination
    const item: IWeight = {
      weight,
      width,
      height: width,
      origin: payload,
      falling: true,
      location: { left, top: 0 },
      type,
      id: null,
      rotation: 0
    };
    commit("addWeight", item);
  },
  tick({ commit, state, getters }): void {
    if (state.gameOver) return;
    commit("move");
    state.weights.forEach((w: IWeight) => {
      if (w.falling) {
        w.location.top += state.fallingRate;
        // check for collision with plank
        const weightPoints = IntersectionDetection.getWeightPoints(w);
        const [plankStart, plankEnd] = getters.plankTopLine;
        const collision = IntersectionDetection.lineRect(
          plankStart,
          plankEnd,
          weightPoints
        );
        if (collision) {
          w.falling = false;
        }
        // check for collision with other weights which are not falling
        const otherWeightsCollision = state.weights
          .filter(e => e.falling === false)
          .reduce((acc: boolean, e: IWeight) => {
            if (acc) return acc;
            return !!IntersectionDetection.rectRect(
              weightPoints,
              IntersectionDetection.getWeightPoints(e)
            );
          }, false);
        if (otherWeightsCollision) w.falling = false;
      }
    });

    // calculate momentum's
    commit("updateMomentums");
    commit("updateRotation");
  }
};

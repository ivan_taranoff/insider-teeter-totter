import { IntersectionDetection } from "@/utils/IntersectionDetection";

describe("IntersectionDetection.ts", () => {
  it("detects line intersection", () => {
    const firstStart = { left: 1, top: 1 };
    const firstEnd = { left: 2, top: 2 };
    const secondStart = { left: 2, top: 1 };
    const secondEnd = { left: 1, top: 2 };
    const result = IntersectionDetection.lineLine(
      firstStart,
      firstEnd,
      secondStart,
      secondEnd
    );

    expect(result).toEqual({ left: 1.5, top: 1.5 });
  });
  it("detects no intersection between lines", () => {
    const firstStart = { left: 1, top: 1 };
    const firstEnd = { left: 2, top: 2 };
    const secondStart = { left: 3, top: 1 };
    const secondEnd = { left: 3, top: 2 };
    const result = IntersectionDetection.lineLine(
      firstStart,
      firstEnd,
      secondStart,
      secondEnd
    );

    expect(result).toEqual(null);
  });
  it("detects line and rectangle intersection", () => {
    const firstStart = { left: 1, top: 1 };
    const firstEnd = { left: 10, top: 10 };
    const rectanglePoints = [
      { left: 3, top: 1 },
      { left: 3, top: 5 },
      { left: 7, top: 5 },
      { left: 7, top: 1 }
    ];
    const result = IntersectionDetection.lineRect(
      firstStart,
      firstEnd,
      rectanglePoints
    );

    expect(result).toContainEqual({ left: 5, top: 5 });
    expect(result).toContainEqual({ left: 3, top: 3 });
  });

  it("detects two rectangles intersection", () => {
    const firstRectangle = [
      { left: 1, top: 1 },
      { left: 1, top: 5 },
      { left: 5, top: 5 },
      { left: 5, top: 1 }
    ];
    const secondRectangle = [
      { left: 3, top: 3 },
      { left: 3, top: 7 },
      { left: 7, top: 7 },
      { left: 7, top: 3 }
    ];
    const result = IntersectionDetection.rectRect(
      firstRectangle,
      secondRectangle
    );

    expect(result).toContainEqual({ left: 3, top: 5 });
    expect(result).toContainEqual({ left: 5, top: 3 });
  });
});

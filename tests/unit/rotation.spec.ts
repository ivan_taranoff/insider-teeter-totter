import { IPoint } from "@/types/IPoint";
import { Rotation } from "@/utils/Rotation";

describe("Rotation.ts", () => {
  it("performs point rotation", () => {
    let point: IPoint = {
      left: 10,
      top: 20
    };

    let pivotPoint: IPoint = { left: 10, top: 10 };

    expect(Rotation.rotate(pivotPoint, point, 90)).toEqual({
      left: 20,
      top: 10
    });
  });
});
